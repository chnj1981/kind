package com.kind.perm.web.system;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kind.common.client.response.JsonResponseResult;
import com.kind.common.client.response.JsonResponseResultList;
import com.kind.common.contants.Constants;
import com.kind.common.dto.DataGridResult;
import com.kind.common.enums.ResponseState;
import com.kind.common.exception.ServiceException;
import com.kind.common.persistence.PageView;
import com.kind.common.uitls.DateUtils;
import com.kind.common.uitls.NumberUtils;
import com.kind.perm.core.system.domain.AreaDO;
import com.kind.perm.core.system.domain.OrganizationDO;
import com.kind.perm.core.system.domain.TableCustomTempletDO;
import com.kind.perm.core.system.service.AreaService;
import com.kind.perm.core.system.service.TableCustomService;
import com.kind.perm.core.system.service.TableCustomTempletService;
import com.kind.perm.web.common.ExcelUtils;
import com.kind.perm.web.common.controller.BaseController;

/**
 * 
 * Function:区域信息管理控制器. <br/>
 * 
 * @date:2016年5月12日 上午11:18:52 <br/>
 * @author 李明
 * @version:
 * @since:JDK 1.7
 */
@Controller
@RequestMapping("system/area")
public class AreaController extends BaseController {
	
	@Autowired
	private AreaService areaService;
	
	
	@Autowired
	private TableCustomService tableCustomService;
	
	@Autowired
	private TableCustomTempletService tableCustomTempletService;

    /**列表页面*/
    private final String LIST_PAGE = "system/area_list";
    /**表单页面*/
    private final String FORM_PAGE = "system/area_form";

	/**
	 * 进入到默认列表页面
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String toListPage() {
		return LIST_PAGE;
	}
	
	/**
	 * 全部区域数据.
	 * @return
	 */
	@RequestMapping(value = "json", method = RequestMethod.GET)
	@ResponseBody
	public List<AreaDO> json(AreaDO query, HttpServletRequest request) {
//		String sort = request.getParameter("sort");
//		String order = request.getParameter("order");
//		query.setOrder(order);
//		query.setSort(sort);
		List<AreaDO> page = areaService.queryList(query);
		return page;
	}
	
	/**
	 * 获取分页查询列表数据.
	 */
	@RequestMapping(value = "selectPageList", method = RequestMethod.GET)
	@ResponseBody
	public DataGridResult selectPageList(AreaDO query, HttpServletRequest request) {
//		String sort = request.getParameter("sort");
//		String order = request.getParameter("order");
//		query.setOrder(order);
//		query.setSort(sort);
		PageView<AreaDO> page = areaService.selectPageList(query);
		return super.buildDataGrid(page);
	}

	/**
	 * 加载添加页面.
	 * 
	 * @param model
	 */
	@RequiresPermissions("sys:area:save")
	@RequestMapping(value = "add", method = RequestMethod.GET)
	public String toAdd(Model model) {
		model.addAttribute("entity", new AreaDO());
		model.addAttribute(Constants.CONTROLLER_ACTION, Constants.CONTROLLER_ACTION_ADD);
		return FORM_PAGE;
	}

	/**
	 * 保存数据.
	 * 
	 * @param entity
	 * @param model
	 */
	@RequestMapping(value = "add", method = RequestMethod.POST)
	@ResponseBody
	public JsonResponseResult add(@Valid AreaDO entity, Model model, HttpServletRequest request) {
        try {
            areaService.save(entity);
            
            //附件管理 star
            //附件管理 end
            
            return JsonResponseResult.success();

        }catch (ServiceException e){
            e.printStackTrace();
            logger.error(e.getMessage());
            return JsonResponseResult.build(ResponseState.ERROR_CODE_SERVICE_ERROR.getCode(), e.getMessage());
        }
	}

	/**
	 * 加载修改页面.
	 * 
	 * @param id
	 * @param model
	 * @return
	 */
	@RequiresPermissions("sys:area:change")
	@RequestMapping(value = "update/{id}", method = RequestMethod.GET)
	public String update(@PathVariable("id") Long id, Model model) {	
		updateView(id, model);
		return FORM_PAGE;
	}

	/**
	 * 修改数据.
	 * 
	 * @param entity
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "update", method = RequestMethod.POST)
	@ResponseBody
	public JsonResponseResult update(@Valid @ModelAttribute @RequestBody AreaDO entity, Model model, HttpServletRequest request) {
        try {
            areaService.save(entity);           
            //附件管理 star
            //附件管理 end       
            return JsonResponseResult.success();

        }catch (ServiceException e){
            e.printStackTrace();
            logger.error(e.getMessage());
            return JsonResponseResult.build(ResponseState.ERROR_CODE_SERVICE_ERROR.getCode(), e.getMessage());
        }
	}

	/**
	 * 删除数据.
	 * 
	 * @param id
	 * @return
	 */
	@RequiresPermissions("sys:area:remove")
	@RequestMapping(value = "remove/{id}")
	@ResponseBody
	public JsonResponseResult remove(@PathVariable("id") Long id) {
	    try {
            if (!NumberUtils.isEmptyLong(id)) {
                areaService.remove(id);
            }
            return JsonResponseResult.success();

        }catch (ServiceException e){
            e.printStackTrace();
            logger.error(e.getMessage());
            return JsonResponseResult.build(ResponseState.ERROR_CODE_SERVICE_ERROR.getCode(), e.getMessage());
        }
	}
	
	/**
	 * 加载查看页面.
	 * 
	 * @param id
	 * @param model
	 * @return
	 */
	@RequiresPermissions("sys:area:view")
	@RequestMapping(value = "view/{id}", method = RequestMethod.GET)
	public String view(@PathVariable("id") Long id, Model model) {
		model.addAttribute("view", "true");
		updateView(id, model);

		return FORM_PAGE;
	}
	
	/**
	 * 导出信息
	 * @param ids
	 * @param request
	 * @param response
	 * @return
	 */
	@RequiresPermissions("sys:area:export")
	@RequestMapping("export")
	@ResponseBody
	public String export(AreaDO query,HttpServletRequest request,HttpServletResponse response){
		TableCustomTempletDO entity = new TableCustomTempletDO();
		entity.setJavaBean("AreaDO");
		List<TableCustomTempletDO> tableCustomTempletlist = tableCustomTempletService.queryList(entity);
		TableCustomTempletDO tableCustomTempletDO = new TableCustomTempletDO();
		if(tableCustomTempletlist.size()>0)
		{
			tableCustomTempletDO = tableCustomTempletlist.get(0);
		}
		else
		{
			return "未设置信息";
		}
		return ExcelUtils.export(areaService.queryList(query), tableCustomService.findTableCustomExport(tableCustomTempletDO.getType()), tableCustomTempletDO.getName()+DateUtils.getDateRandom()+".xls", request, response);
	
	}
	
	private void updateView(Long id, Model model) {
		model.addAttribute("entity", areaService.getById(id));
		model.addAttribute(Constants.CONTROLLER_ACTION, Constants.CONTROLLER_ACTION_UPDATE);
		//获取附件列表   start
		//获取附件列表   end
	}
	
	/**
	 * 根据父AreaId 查询子Area
	 * @param ProvinceId
	 * @return
	 */
	@RequestMapping(value = "selectAreaByParentId/{parentId}")
	@ResponseBody
	public JsonResponseResultList<AreaDO> selectAreaByParentId(@PathVariable("parentId") Long parentId) {
	    try {
	    	List<AreaDO> areaList = new ArrayList<AreaDO>();
            if (!NumberUtils.isEmptyLong(parentId)) {
            	
            	AreaDO area = areaService.getById(parentId);
            	
                areaList = areaService.queryByPcode(area.getCode());
            }
            return new JsonResponseResultList<AreaDO>(areaList);
        }catch (ServiceException e){
            e.printStackTrace();
            logger.error(e.getMessage());
            return new JsonResponseResultList<AreaDO>(-1, e.getMessage());
        }
	}

}
