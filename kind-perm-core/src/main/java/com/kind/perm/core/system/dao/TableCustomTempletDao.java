/**
 * Project Name:kafa-wheat-core
 * File Name:UserRoleDao.java
 * Package Name:com.kind.perm.core.dao
 * Date:2016年6月14日下午5:15:06
 * Copyright (c) 2016, http://www.mcake.com All Rights Reserved.
 *
*/

package com.kind.perm.core.system.dao;

import java.util.List;

import com.kind.common.persistence.PageQuery;
import com.kind.perm.core.system.domain.TableCustomTempletDO;

/**
 * Function:导出数据模板持久化接口. <br/>
 * Date: 2017年02月04日 上午10:15:06 <br/>
 * 
 * @author 李明
 * @version
 * @since JDK 1.7
 * @see
 */
public interface TableCustomTempletDao {
	public static final String NAMESPACE = "com.kind.perm.core.mapper.system.TableCustomTempletDOMapper.";

	/**
	 * 分页查询的数据<br/>
	 *
	 * @param pageQuery
	 * @return
	 */
	List<TableCustomTempletDO> page(PageQuery pageQuery);

	/**
	 * 分页查询的数据记录数<br/>
	 *
	 * @param pageQuery
	 * @return
	 */
	int count(PageQuery pageQuery);

	/**
	 * [保存/修改] 数据<br/>
	 *
	 * @param entity
	 */
	int saveOrUpdate(TableCustomTempletDO entity);

	/**
	 * 根据id获取数据对象<br/>
	 *
	 * @param id
	 * @return
	 */
	TableCustomTempletDO getById(Long id);

    /**
     * 删除数据 <br/>
     *
     * @param id
     */
    void remove(Long id);
    
	
	/**
	 * 查询符合条件的所有数据
	 * @param entity
	 * @return
	 */
	List<TableCustomTempletDO> queryList(TableCustomTempletDO entity);

}
