package com.kind.perm.core.system.dao.impl;


import java.io.Serializable;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.kind.common.datasource.KindDataSourceHolder;
import com.kind.common.datasource.DataSourceKey;
import com.kind.common.persistence.PageQuery;
import com.kind.common.persistence.mybatis.BaseDaoMyBatisImpl;
import com.kind.perm.core.system.dao.AreaDao;
import com.kind.perm.core.system.domain.AreaDO;

/**
 * 
 * 区域信息数据访问实现类. <br/>
 * 
 * @date:2017-03-01 09:53:29 <br/>
 * @author 李明
 * @version:
 * @since:JDK 1.7
 */
@Repository
public class AreaDaoImpl extends BaseDaoMyBatisImpl<AreaDO, Serializable> implements AreaDao {

	@Override
	public List<AreaDO> page(PageQuery pageQuery) {
		return super.query(NAMESPACE + "page", pageQuery);
	}

	@Override
	public int count(PageQuery pageQuery) {
		return super.count(NAMESPACE + "count", pageQuery);
	}

	@Override
	public int saveOrUpdate(AreaDO entity) {
		//KindDataSourceHolder.setDataSourceKey(DataSourceKey.YM_ORDER_DATA_SOURCE);
		if (entity.getId() == null) {
			return super.insert(NAMESPACE + "insert", entity);
		} else {
			return super.update(NAMESPACE + "update", entity);
		}
	}

	@Override
	public void remove(Long id) {
		delete(NAMESPACE + "delete", id);
	}

	@Override
	public AreaDO getById(Long id) {
		return super.getById(NAMESPACE + "getById", id);
	}

	@Override
	public List<AreaDO> queryList(AreaDO entity) {
		return super.query(NAMESPACE + "queryList", entity);
	}

	@Override
	public List<AreaDO> queryByPcode(String pcode) {
		return super.query(NAMESPACE + "queryByPcode", pcode);
	}

	@Override
	public AreaDO queryByCode(String code) {
		return super.query(NAMESPACE + "queryByCode", code).get(0);
	}
}
