/**  
 * @Project: jxoa
 * @Title: FileModel.java
 * @Package com.oa.commons.model
 * @date 2013-4-22 上午8:55:17
 * @Copyright: 2013 
 */
package com.kind.perm.core.system.domain;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 类名：CoFileDO
 * 功能：文件分类保存模型
 * 详细：
 * 作者：李明
 * 版本：1.0
 * 日期：2017-1-13 下午4:55:17
 *
 */
public class CoFileDO implements Serializable {
	
	
	private Long id;
	
	/**
	 * 默认类型:file
	 */
	private String objectA;
	
	/**
	 * 默认行的Id
	 */
	private Long objectAId;
	
	/**
	 * 关联的业务：
	 */
	private String objectB;
	
	/**
	 * 关联的业务id
	 */
	private Long objectBId;
	
	private Date createTime;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getObjectA() {
		return objectA;
	}

	public void setObjectA(String objectA) {
		this.objectA = objectA;
	}

	public Long getObjectAId() {
		return objectAId;
	}

	public void setObjectAId(Long objectAId) {
		this.objectAId = objectAId;
	}

	public String getObjectB() {
		return objectB;
	}

	public void setObjectB(String objectB) {
		this.objectB = objectB;
	}

	public Long getObjectBId() {
		return objectBId;
	}

	public void setObjectBId(Long objectBId) {
		this.objectBId = objectBId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}



	
	
	
	
}
